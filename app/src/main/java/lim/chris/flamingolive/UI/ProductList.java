package lim.chris.flamingolive.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import lim.chris.flamingolive.R;
import lim.chris.flamingolive.database.Repository;
import lim.chris.flamingolive.entities.Part;
import lim.chris.flamingolive.entities.Product;

public class ProductList extends AppCompatActivity {
    private Repository repository;
    int productID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductList.this, ProductDetails.class);
                startActivity(intent);
            }
        });
        RecyclerView recyclerView=findViewById(R.id.recyclerview);
        repository=new Repository(getApplication());
        List<Product> allProducts=repository.getmAllProducts();
        final ProductAdapter productAdapter=new ProductAdapter(this);
        recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        productAdapter.setProducts(allProducts);
        //System.out.println(getIntent().getStringExtra("test"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_product_list, menu);
        return true;

    }
    @Override
    protected void onResume() {

        super.onResume();
        RecyclerView recyclerView=findViewById(R.id.recyclerview);
        final ProductAdapter productAdapter=new ProductAdapter(this);
        recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<Product> allProducts=repository.getmAllProducts();
        productAdapter.setProducts(allProducts);
//        List<Product> allProducts = new ArrayList<>();
//        productAdapter.setProducts(allProducts);
//        for (Product product : repository.getmAllProducts()) {
//            if (product.getProductID() == productID) {
//                allProducts.add(product);
//            }
//        }
//        productAdapter.setProducts(allProducts);
    }



    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mysample) {
            repository=new Repository(getApplication());
            //Toast.makeText(ProductList.this,"put in sample data",Toast.LENGTH_LONG).show();
            Product product = new Product(0, "bicycle", 100.0);
            repository.insert(product);
            product = new Product(0, "tricycle", 100.0);
            repository.insert(product);
            Part part=new Part(0,"wheel",10,1);
            repository.insert(part);
            part=new Part(0,"pedal",10,1);
            repository.insert(part);
            Toast.makeText(ProductList.this,"Added new Product List",Toast.LENGTH_LONG).show();
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            //Intent intent=new Intent(ProductList.this,ProductDetails.class);
            //startActivity(intent);
            return true;
        }
        return true;
    }

}